import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Inkline from '@inkline/inkline'
import '@inkline/inkline/dist/inkline.css'
import i18n from './i18n'
import Vuebar from 'vuebar'

Vue.config.productionTip = false

Vue.use(Inkline)
Vue.use(Vuebar)

new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')
